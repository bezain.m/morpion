#import

from random import randint
from tkinter import *
import time
import os


def initialiser():
    #cette fonction permet de creer les 'cases' du jeu sous forme de boutons
    for ligne in range(3):
        boutons.append([])
        for colonne in range(3):
            boutons[ligne].append(Button(fenetre,text="",bg="grey",image=fond,command=lambda l=ligne,c=colonne: jouer(l,c)))
            boutons[ligne][colonne].grid(row=ligne,column=colonne)



def fin():
    #fenêtre de fin
    global done
    done=True
    time.sleep(1)
    try :

        fenetre.destroy() #ferme le jeu
    except :
        pass


    if nul : #personne n'a gagné
        texte = "La partie est nulle !"

    else: #un joueur à gagné
        Joueur = 'O'
        if numJoueur == -1:
            Joueur = 'X'
        texte="Le joueur " +Joueur+ " a gagne !"

    gagne = Tk()
    gagne.title("Bravo !")
    gagne.geometry("790x260")
    gagne.configure(bg=('#b6ccff'))
    label = Label(gagne, text=texte, font=("Courier",35)).place(x=100, y=20)

    # quit
    Button(gagne, text="Quitter le jeu",command=lambda : gagne.destroy(), font=("Terminal",25), activeforeground="red").place(x=200, y=90)


    # rejouer
    Button(gagne, text="rejouer",command=lambda : replay(gagne), font=("Terminal",15), activeforeground="red").place(x=350,y=200)

    gagne.mainloop()


#permet de relancer le jeu du début
def replay(gagne):
    global go_for_replay
    gagne.destroy()
    go_for_replay = True



#insertion des deux joueurs
def jouer(l,c):
    global numJoueur


    if grille[l][c]==0:
        grille[l][c]=numJoueur
        numJoueur=-numJoueur
        afficher(l,c)

    print("\ngrille : ")

    for i in range(3):
        print(grille[i])


#toute les fins possibles
def si_fin():
    global nul

    if grille[0][0]==grille[1][1]==grille[2][2]!=0 or grille[0][2]==grille[1][1]==grille[2][0]!=0:
        fin()

    for i in range(len(grille)):
        if grille[i][0]==grille[i][1]==grille[i][2]!=0 or grille[0][i]==grille[1][i]==grille[2][i]!=0:
            fin()

    done = True

    for i in range(3):
        for j in range(3):
            if grille[i][j] == 0:
                done = False


    if done == True:
        nul = True
        fin()




#affichage des dessins (croix et rond)
def afficher(l,c):
    if grille[l][c]==1:
        boutons[l][c].configure(image=croix)
    else :
        boutons[l][c].configure(image=rond)

def start_game():
    global done
    done=False

def quit():
    global done, exit
    done=False
    exit=True



#menu pour demarrer le jeu
def menu():
    global done
    menu_screen=Tk()
    canvas = Canvas(menu_screen, width = 500, height = 500)
    canvas.pack()
    img = PhotoImage(file="accueil.gif")
    canvas.create_image(0,-40, anchor=NW, image=img)
    menu_screen.title("Bienvenue")
    menu_screen.geometry("500x460")
    start = False
    start = Button(menu_screen, text="START", relief="ridge" ,font=("Terminal",20), activeforeground="red", activebackground="light gray", bd="5",command=lambda : start_game()).place(x=350, y=260)
    end = Button(menu_screen, text="EXIT", relief="ridge", bd="5",font=("Terminal",20), activeforeground="red", activebackground="light gray",  command=lambda : quit()).place(x=80, y=350)
    done = True
    while done:
        menu_screen.update_idletasks()
        menu_screen.update()
        time.sleep(0.1)
    menu_screen.destroy()



#variables
done=True
exit=False
menu()

grille = [[0,0,0],
          [0,0,0],
          [0,0,0]]

nul = False
go_for_replay = False
boutons=[]
numJoueur=1



#fenetre principale du jeu
fenetre = Tk()
fenetre.title("Le Morpion")
fenetre.geometry("615x605")


#dessins
croix = PhotoImage(file='croix.gif')
rond = PhotoImage(file='rond.gif')
fond = PhotoImage(file='bl.gif')


initialiser()


if exit==True:
    done=True

#mainloop
while not done:
    fenetre.update_idletasks()
    fenetre.update()
    si_fin()
    time.sleep(0.1)



if go_for_replay:
    exec(open("./main.py").read())













